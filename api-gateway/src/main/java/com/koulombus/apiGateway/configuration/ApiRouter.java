package com.koulombus.apiGateway.configuration;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiRouter {

    /**
     * Check this: Spring Cloud Gateway Dynamic Routes from Database
     * https://medium.com/bliblidotcom-techblog/spring-cloud-gateway-dynamic-routes-from-database-dc938c6665de
     */
    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes() //
                      .route("customer", r -> r.path("/api/v1/customers/**").uri("lb://CUSTOMER")) //
                      .build();
    }
}
