# Getting Started

### Reference Documentation

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.6.3/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.6.3/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.6.3/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides

The following guides illustrate how to use some features concretely:

* [Building a RESTFUL Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

### Create banner text:
[Create banner text](https://devops.datenkollektiv.de/banner.txt/index.html)



### Postgres
If you want to run Postgres in Docker, the just crate a file in the root of your project and create the file "
docker-compose.yml". docker-compose.yml:

```yml
    servcies:
      postgres:
        container_name: postgres
        image: postgress
        environment:
          POSTGRES_USER: user
          POSTGRES_PASSWORD: password
          PGDATA: /data/postgres
        volumes:
          - postgress:/data/postgres
        ports:
          - "5432-5432"
        networks:
          - postgres
        restart: unless-stopped

        pgadmin:
          container_name: postgres
          image: dpage/pgadmin4
          environment:
            PGADMin_DEFAULT_EMAIL: ${PGADMin_DEFAULT_EMAIL:-pgadmin4}
            PGADMin_DEFAULT_PASSWORD: ${PGADMin_DEFAULT_PASSWORD:-admin}
            PGADMin_CONFIG_SERVER_MODE: 'false'
          volumes:
            - pgadmin:/var/lib/pgadmin

          ports:
            - "5050:80"
          networks:
            - postgres
          restart: unless-stopped

    networks:
      postgrs:
        driver: bridge
        
    volumes:
      postgres:
      pgadmin:
```

## Docker

### Advanced Docker Compose Configuration
Start different environments or "stacks" of your Compose application running independently of each other. This can handle with the --project-name or -p option to docker-compose. An example would be something like this:

Launch application version 1: docker-compose -p appv1 up -d
Launch application version 2: docker-compose -p appv2 up -d
Launch application version 3: docker-compose -p appv3 up -d


### Docker configuration file
Edit the docker configuration file.
```
vi ~/.docker/config.json
```

### Docker for Zipkin Server
Start a docker image standalone for:
```
docker run -d -p 9411:9411 openzipkin/zipkin
```
### Docker for RabbitMQ
RabbitMQ:
```
docker run -d -p 5672:5672 -p 15672:15672  rabbitmq:3.9.11-management-alpine
```
RabbitMQ username and password:
```
username: guest
password: guest
```
### Build docker image and push to Docker-Hub
for a specific service, e.g. customer:
```
cd customer
mvn clean package -P build-docker-image
```
or in one go in the root folder:
```
mvn clean package -P build-docker-image
```
After push the docker images to docker hub, we have to pull, because we use the version "latest". Check that in the file ```docker-compose.yml``` section ```customer.image``` e.g. ```koulombus/customer:latest```
```
docker compose pull
```
Bring the Docker containers now up and running.
```
docker compose up -d
```

Check Docker instances:
```
docker ps --format=$FORMAT
```
Stop all docker instances
```
docker compose stop
```
Reading all down
```
docker compose down
```
### Eureka Server
Open Dashboard ```http://localhost:8761/```

### Zipkin
Open Dashboard ```http://localhost:9411/zipkin```

### RabbitMQ
Open Dashboard ```http://localhost:15672/#/queues```


### Application.properties (profile)
All after the dash from the word application calls profile.
```clients-application-docker.properties```
in this example, the name docker is the profile.
To select the correct application.properties in the docker file, just use the properties ```environment```.
```yaml
environment:
      - SPRING_PROFILES_ACTIVE=docker
```


## Minikube

install minikube
```
brew install minikube
```
Check version
```
minikube version

minikube version: v1.25.2
commit: 362d5fdc0a3dbee389b3d3f1034e8023e72bd3a7
```
Start minikube with Memory of 5G
```
minikube start --memory=4g

😄  minikube v1.25.2 on Darwin 10.15.7
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.23.3 preload ...
    > preloaded-images-k8s-v17-v1...: 505.68 MiB / 505.68 MiB  100.00% 38.03 Mi
    > gcr.io/k8s-minikube/kicbase: 379.06 MiB / 379.06 MiB  100.00% 9.99 MiB p/
🔥  Creating docker container (CPUs=2, Memory=4096MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

Get status of minikube
```
minikube status

minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Check the IP of the installed minikube:
```
minikube ip

> 192.168.49.2
```

The IP of the Control plane (Master node) is 192.168.49.2

Use kubectl to communicate with the cluster.
[Install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-macos/)

## kubectl

install with homebrew
```
brew install kubectl 
```

Make the kubectl binary executable.
```cmd
chmod +x ./kubectl
```

Move the kubectl binary to a file location on your system PATH.
```
sudo mv ./kubectl /usr/local/bin/kubectl
sudo chown root: /usr/local/bin/kubectl
```

Check kubectl version
```
kubectl version --client

Client Version: version.Info {
	Major: "1",
	Minor: "23",
	GitVersion: "v1.23.4",
	GitCommit: "e6c093d87ea4cbb530a7b2ae91e54c0842d8308a",
	GitTreeState: "clean",
	BuildDate: "2022-02-16T12:38:05Z",
	GoVersion: "go1.17.7",
	Compiler: "gc",
	Platform: "darwin/amd64"
}
```

## Some important keywords
Things you gave to know before we start.
### ClusterIP Services:
>Exposes the service on a cluster-internal IP. Choosing this value makes the service only reachable from within the cluster. This is the default.

### NodePort:
>Exposes the service on each Node’s IP at a static port (the NodePort). A ClusterIP service, to which the NodePort service will route, is automatically created. You’ll be able to contact the NodePort service, from outside the cluster, by requesting ```<NodeIP>:<NodePort>```.

### Deployments vs StatefulSets

- A deployment is a Kubernetes object which is preferred when deploying a stateless application or when multiple replicas of pods can use the same volume.
- A stateful set is a Kubernetes object which is preferred when we require each pod to have its own independent state and use its own individual volume.

Another major difference between them is the naming convention for pods.

- ***case of deployments***, pods are always assigned a unique name but this unique name ***changes after the pod are deleted & recreated***.
- In the ***case of the stateful set*** – each pod is assigned a unique name and this ***unique name stays with it even if the pod is deleted & recreated***.


### Create a pod
e,g,
pod_name = eureka-server
docker_hub_image_path = koulombus/eureka-server:latest
```
kubectl run [pod_name] --image=[docker_hub_image_path] --port=80
```

### Apply all yml
apply all in a folder e.g./bootstrap/mongodb/**. Go to folder /k8s/minikube and run following command.

```
kubectl apply -f bootstrap/mongodb


configmap/mongo-config unchanged
service/mongo-creds created
statefulset.apps/mongo configured
persistentvolume/mongo-data unchanged
persistentvolumeclaim/pvc unchanged
```

#### Get all pods
```
kubectl get pods

NAME         READY   STATUS    RESTARTS      AGE
mongodb-0    1/1     Running   0             12s
postgres-0   1/1     Running   1 (22h ago)   28h
rabbitmq-0   1/1     Running   1 (22h ago)   25h
zipkin-0     1/1     Running   1 (22h ago)   25h
```


#### Port forwarding (JUST FOR TESTING!!)
```
kubctl port-forward pod/[pod_name] [host_port_from]:[to_inside_container]
```
e.g.
```
kubctl port-forward pod/eureka-server 8080:8761
```

### Describe the pod and show the events
```
kubectl describe pod postgres-0


Events:
  Type     Reason            Age                  From               Message
  ----     ------            ----                 ----               -------
  Warning  FailedScheduling  10m                  default-scheduler  0/1 nodes are available: 1 persistentvolumeclaim "postgres-pc-volume-claim" not found.
  Warning  FailedScheduling  4m59s (x6 over 10m)  default-scheduler  0/1 nodes are available: 1 pod has unbound immediate PersistentVolumeClaims.
  Normal   Scheduled         4m48s                default-scheduler  Successfully assigned default/postgres-0 to minikube
  Normal   Pulling           4m47s                kubelet            Pulling image "postgres"
  Normal   Pulled            4m32s                kubelet            Successfully pulled image "postgres" in 14.9654016s
  Normal   Created           4m32s                kubelet            Created container postgres
  Normal   Started           4m32s                kubelet            Started container postgres

```

Check logs
```
kubectl logs postgres-0



The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "en_US.utf8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

.....
```

Open SSH shell to minikube and check if the folder data for the storage is created.

```
minikube ssh


docker@minikube:~$ cd /
docker@minikube:~$ cd /mnt/
docker@minikube:~$ ls -alf
..  .  data

```

Create now a database in the pod postgres-0.

first open the psql console.
Params:
    -U = user, check the file configmap.yml => data.POSTGRES_USER
```
kubectl exec -it postgres-0 -- psql -U koulombus


psql (14.2 (Debian 14.2-1.pgdg110+1))
Type "help" for help.

```

show a list of databases
```
\l


                                  List of databases
   Name    |   Owner   | Encoding |  Collate   |   Ctype    |    Access privileges
-----------+-----------+----------+------------+------------+-------------------------
 koulombus | koulombus | UTF8     | en_US.utf8 | en_US.utf8 |
 postgres  | koulombus | UTF8     | en_US.utf8 | en_US.utf8 |
 template0 | koulombus | UTF8     | en_US.utf8 | en_US.utf8 | =c/koulombus           +
           |           |          |            |            | koulombus=CTc/koulombus
 template1 | koulombus | UTF8     | en_US.utf8 | en_US.utf8 | =c/koulombus           +
           |           |          |            |            | koulombus=CTc/koulombus
(4 rows)
```

Create database for customer
```
create database customer;
```

Create database for fraud
```
create database fraud;
```
Create database for notification
```
create database notification;
```

>CTRL-d to execute the console.







### Get all resources
```
kubectl get all


NAME             READY   STATUS    RESTARTS   AGE
pod/mongo-0      0/1     Pending   0          28m
pod/postgres-0   1/1     Running   0          3h43m
pod/rabbitmq-0   1/1     Running   0          3m29s
pod/zipkin-0     1/1     Running   0          3m36s

NAME                  TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                          AGE
service/kubernetes    ClusterIP      10.96.0.1        <none>        443/TCP                          4d
service/mongo-creds   ClusterIP      None             <none>        27017/TCP                        28m
service/postgres      ClusterIP      10.101.137.62    <none>        5432/TCP                         3h43m
service/rabbitmq      NodePort       10.109.108.109   <none>        15672:31672/TCP,5672:30672/TCP   3m29s
service/zipkin        LoadBalancer   10.110.121.138   <pending>     9411:31623/TCP                   3m36s

NAME                        READY   AGE
statefulset.apps/mongo      0/1     28m
statefulset.apps/postgres   1/1     3h43m
statefulset.apps/rabbitmq   1/1     3m29s
statefulset.apps/zipkin     1/1     3m36s
```
so, access now e.g. to rabitmq....
```
minikube service rabitmq --url
```

### MongoDB (just for local testing)
Create MongoDB Secrets

Create the Secret.
```
kubectl apply -f bootstrap/mongodb/secrets.yml
```
>Tip: Decoding contents of Secret objects

Kubernetes stores the content of all secrets in a base 64 encoded format. If you want to see how your string will appear in a base64 format, execute the following.
```
echo "devopscube" | base64 
//after encoding it, this becomes ZGV2b3BzY3ViZQo=
```
If you want to decode a base64 string. Run

```
echo "ZGV2b3BzY3ViZQo=" | base64 --decode
//after decoding it, this will give devopscube
```

Create MongoDB Persistent Volume

***PersistentVolumes (PV):*** are objects which map to a storage location. It’s a piece of storage in the cluster that has been provisioned by an administrator.

***Persistent Volume Claims (PVC):*** are Kubernetes objects that act as requests for storage. Kubernetes looks for a PV from which space can be claimed and assigned for a PVC. PVC works only if you have dynamic volume provisioning enabled in the Kubernetes cluster.
